Fix 'WannaCry'
===================

Multiple files to assist with 'WannaCry' vulnerability remediation

**Summary:**

- disableSMB1_v1.0
    - Remediates Issues On: Windows 7, Windows 8, Windows 8.1, Windows 10, Server 2008, Server 2008 R2, Server 2012, Server 2012 R2, Server 2016
    - Disables the SMB1 Protocol on Microsoft Windows Systems. Requires Windows PowerShell on the affected host in order to function.
    - Various tools may be used to push this script out. These may include Microsoft tools like System Center Configuration Manager (SCCM), or other system configuration and mangement toolsets.
    - Many, but not all of the systems affected will require a reboot after this script has run in order for the changes to be completed. This script does *not* force a reboot, so that may be done in occordance with your companies service windows.
    - Script must be run as Administrator in order to perform service actions.

**License:**

MIT License

Copyright (c) 2017 Will Udovich

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.