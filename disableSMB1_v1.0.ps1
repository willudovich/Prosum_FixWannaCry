﻿#Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol

# PowerShell may be installed on Windows XP/Server2003, but probably isn't.
# You'll want to patch those systems manually. Make sure to use SCCM or something else to do so.
# Use SMB1 Blocking GPO
# Use batch file
# Commands came from here: https://support.microsoft.com/en-us/help/2696547/how-to-enable-and-disable-smbv1,-smbv2,-and-smbv3-in-windows-vista,-windows-server-2008,-windows-7,-windows-server-2008-r2,-windows-8,-and-windows-server-2012
# https://blogs.technet.microsoft.com/mmpc/2017/05/12/wannacrypt-ransomware-worm-targets-out-of-date-systems
# Updates for Windows 8 and Server 2008 Downloaded From: http://www.catalog.update.microsoft.com/Search.aspx?q=KB4012598
# Updates for Windows 7 and Server 2008 R2 Downloaded From: http://www.catalog.update.microsoft.com/Search.aspx?q=KB4012212
# Updates for Windows 8.1 and Server 2012 R2 Downloaded From: http://catalog.update.microsoft.com/v7/site/Search.aspx?q=KB4012213

$OSVersion = (gwmi win32_operatingsystem).caption

#Try this for everything - it works well
try{
    #SMB1 Client Component
    Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol -NoRestart
}
catch{
    $windowsFeatureDisableError = $_
}


if ($OSVersion -match "Windows 10"){
    #SMB1 Server Component
    Remove-WindowsFeature FS-SMB1

    #SMB1 Client Component
    Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol -NoRestart
}
elseif ($OSVersion -match "Windows 8"){
    # No need for a computer restsart
    # This disables SMB1 in the server components of the OS
    Set-SmbServerConfiguration -EnableSMB1Protocol $false -Force

    &sc.exe config lanmanworkstation depend= bowser/mrxsmb20/nsi
    &sc.exe config mrxsmb10 start= disabled
}
elseif ($OSVersion -match "Windows 7"){
    # The computer must be restarted after applying this registry key!
    # This key disables SMB1 in the server components of the OS
    Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" SMB1 -Type DWORD -Value 0 -Force

    &sc.exe config lanmanworkstation depend= bowser/mrxsmb20/nsi
    &sc.exe config mrxsmb10 start= disabled
}
elseif (($OSVersion -match "Server 2008") -or ($OSVersion -match "Server 2008 R2")){
    # The computer must be restarted after applying this registry key!
    # This key disables SMB1 in the server components of the OS
    Set-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\Services\LanmanServer\Parameters" SMB1 -Type DWORD -Value 0 -Force

    &sc.exe config lanmanworkstation depend= bowser/mrxsmb20/nsi
    &sc.exe config mrxsmb10 start= disabled
}
elseif ($OSVersion -match "Server 2012"){
    # No need for a computer restart
    # This disables SMB1 in the server components of the OS
    Set-SmbServerConfiguration -EnableSMB1Protocol $false -Force

    &sc.exe config lanmanworkstation depend= bowser/mrxsmb20/nsi
    &sc.exe config mrxsmb10 start= disabled
}
elseif ($OSVersion -match "Server 2016"){
    #SMB1 Server Component
    Remove-WindowsFeature FS-SMB1

    #SMB1 Client Component
    Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol -NoRestart

}
